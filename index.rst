.. cs101 documentation master file, created by
   sphinx-quickstart on Tue May 14 14:55:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CS 101 Course Operations and Policy Handbook
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
