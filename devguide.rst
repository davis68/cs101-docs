Course Developer's Guide
========================


Compass
-------

 Course Tools→Batch Create Grade Columns


Miscellaneous
-------------

Short URLs, using the ``go.illinois.edu`` or ``go.cs.illinois.edu`` should be used whenever a URL is difficult to type.

