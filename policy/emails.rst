Email Templates
===============

Email:  Course Aide Enquiry
-------------------

Hello, <name>.  We'll be happy to have you join the CA team with the course.

We expect about 5 hours a week plus an hour of prep time.  I'll send further details once I have a hiring budget for the semester.


Email:  Missed Lab (Valid Excuse)
---------------------------------

Hello <name>.  Lab makeup can be done during office hour in the week of the missed lab or the following week.  Please check the `office hour schedule <https://relate.cs.illinois.edu/course/cs101-fa19/#grading>`_ and let me know when you plan to go in.


Email:  Missed Lab (Valid Excuse without Documentation)
-------------------------------------------------------

Hello <name>.  To arrange a lab makeup, we need documentation for your absence.  Can you email us a dean's letter, doctor's note, or something similar?

Once you do, we can arrange you to make up your lab during office hour next week. You can check the `office hour schedule <https://relate.cs.illinois.edu/course/cs101-fa19/#grading>`_ and let me know when you plan to go in.

Email:  Missed Lab (Invalid Excuse)
-----------------------------------

Hello <name>.  According to CS 101's course policy, lab make-up is provided for approved absences with documents such as a doctor’s note, dean’s letter, etc.  Unfortunately, <event> does not meet this requirement.  We do, however, drop the lowest lab grade in final grading.

Email:  Missed Lab (Too Late)
-----------------------------

Hello <name>.  Unfortunately, since we have released grades already for that lab, you are no longer able to make it up.

Email:  Missed Lab (Coordinating)
---------------------------------

Hello <name>.  Please note that it is your responsibility to submit the lab and make sure your grade is uploaded on compass before the end of the semester.

<ta_name>, <name> (NetID <netid>) in <section> has scheduled to make up <lab> on <date> during office hours.  Please check for the student’s grade and upload it.

Email:  Missed Class
--------------------

Hello <name>.  We don’t track absences for lecture attendance in CS 101 at all.  Instead, we forgive upt to six absences during final grading.

Email:  CA Recruitment (General Call)
-------------------------------------

CS 101 is seeking qualified course aides.  Since you did well in the course last semester, particularly in the labs, I would like to invite you to participate.

If you are able to assist at this time and are interested in doing so, please reply to this email (cc ``cs101admin@cs.illinois.edu``) and we'll be in touch to schedule you.

First-semester CA positions with CS 101 pay $10/hour, and we anticipate that the hours per week will remain constant at five lab and office hours plus one preparation hour.  We will begin employing CAs during the third week of class, so please reply quickly if you are willing and able.  Thank you.

Email:  CA Recruitment (Response to Enquiry)
--------------------------------------------

Hello <name>.  We'll be happy to have you join the CA team.

We expect about five hours of work per week.  I'll send further details once I have a hiring budget for the semester.

We pay $10/hr for first-time CAs, $11/hr for returning CAs.

Email:  CA Recruitment (Scheduling Returning)
---------------------------------------------

Thank you for joining CS 101 this semester!

To get started on scheduling for this semester, please read the instructions below and fill in your availabilities via `this spreadsheet <>`_.

Here’s how this sheet works:

- Like last semester, we need two CAs for each lab & office hour session.  Please put your names next to any sessions you are available for.  (This doesn’t necessarily mean you will be assigned to all of the sessions you are listed for.  We will make adjustments based on everyone’s schedules.)

- If there are already two people signed up for your preferred session, you can put your name down in the waitlist column.

- Ideally, we wish to have at least one returning CA for each session, so when you fill in the sheet, please take that into consideration.

CAs are expected to start working from the third week of school.  I hope to get the schedule ready and recruit new CAs based on your schedules soon, so please share your availability at your earliest convenience.

Email:  CA Recruitment (Scheduled)
----------------------------------

Hello all.  Please see below for a full schedule for this semester.

All CAs are expected to begin from next week. Here are a few things before you all start.

- Please use `this link <TODO>`_ to join the course Slack team.  (For those who are new to this, it’s a team messaging app.)  Once you have joined, please check the channel ``#course-aides``.

- Please be punctual and perform your responsibilities as course aides throughout the semester.  It is your responsibility to let the TA know that you are there to help him or her.  (If the TA reports your absence, it’s difficult for us to tell whether you attended the session or not.)

- If you need to miss a shift, it is also your responsibility to find someone to cover for you or trade with you.  Use the ``#course-aides`` Slack channel to coordinate this.  You can view the TA assignments in the schedule below, and all TAs’ email addresses are on the course website at `go.illinois.edu/cs101 <https://go.illinois.edu/cs101>`_.

- Submit your timesheet on time.  You can view the payroll schedule `here <https://my.cs.illinois.edu/timetracker/PayrollSchedule>`_.

Thank you!

Email:  CA Recruitment (No more needed)
---------------------------------------

Hello <name>.  Unfortunately we have heard back from enough new applicants and returning course aides to cover all of the positions this semester.  We welcome you to apply again for next semester.

Email:  TA Recruitment (Redirection)
------------------------------------

Hello, <name>.  I'd be happy to have you join the CS 101 team.  The CS department handles TA applications directly.  Please contact them with regards to your interest in a position.
