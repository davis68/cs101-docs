CS 101 Course Operations Manual
===============================

.. toctree::
   :maxdepth: 1

   intro/ta
   intro/ca

   checklist/startup
   checklist/labs
   checklist/quiz
   checklist/shutdown
   checklist/ta-startup
   checklist/ca-startup

   labguides/lab00
   labguides/lab01
   labguides/lab02
   labguides/lab03
   labguides/lab04
   labguides/lab05
   labguides/lab06
   labguides/lab07
   labguides/lab08
   labguides/lab09
   labguides/lab10
   labguides/lab11
   labguides/lab12

   stats
   outline

   policy/policies
   policy/emails

   devel/relate
   devel/philosophy
   devel/todo

   devguide
   styleguide
