
Our guiding principles for operating the course are:

1.  Never walk by a mistake.
2.  Mind your own business.
3.  


We prioritize accessibility, 
We prioritize clarity of expression and code, even if the result isn't "Pythonic"; notably, we do not cover comprehensions or classes, so please make no reference to these.

More principles of good teaching can be read at the following sites:

-  Greg Wilson, `Teaching Tech Together <http://teachtogether.tech/en/>`_


