To-Do List
==========

- Generate flowchart images for Anki flash card deck.
- Convert all images to TikZ plots.
- 


Wishlist
========

- SymPy-like interactive cells
- 

Outcomes
========

- lectures:
    - activity, peer instruction, i>clicker questions
- labs:
    - guided-enquiry w/ persistent teams
    - have TAs observe each other through lab02
- quizzes:
    - broader question bank
- homework:
    - add MATLAB as flows
- lessons:
    - fill in questions more, more flowchart matching, more recomposition
    - Rewrite the following program so that it uses (one more, one less) variable.

