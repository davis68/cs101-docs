Staff Meetings
==============

- continue to upgrade staff meetings, https://namhenderson.wordpress.com/2018/05/11/re-effective-meetings/



ta0
---
- Course Operations Handbook
- Instructor Notes for `lab00`, https://github.com/UI-CS101/lab-notes
- Coordinate observations between TAs

ta1
---
- Instructor Notes for `lab01`
- Watch [Teaching Python:  The Hard Parts](https://www.youtube.com/watch?v=CjYEpVNbM-s) on your own beforehand.
- Review student policies, prepare for teams
- Dalio's _Principles_; managing CAs and team

ta2
---
- Instructor Notes for `lab02`, `lab03`
- Activity generation
- Grading, feedback, & Compass uploading

ta3
---
- Instructor Notes for `lab04`, `lab05`
- Extreme Ownership

ta4
---
- Grade Check #1
- Monitoring cheating

ta5
---
- Instructor Notes for `lab06`, `lab07`
- `Teaching Tech Together <http://teachtogether.tech/en/>`_

ta6
---
- Instructor Notes for `lab08`, `lab09`
- `Seven Ways <http://third-bit.com/2018/03/16/seven-ways.html>`_

ta7
---
- Instructor Notes for `lab10`
- TA Feedback on Python

ta8
---
- Instructor Notes for `lab11`, `lab12`
- Teaching MATLAB
- Grade Check #2
- TA Feedback on Course (One-Up/One-Down)
