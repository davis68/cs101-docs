Checklist:  Startup for CAs
===========================

1.  Make sure that you receive notifications from the course Slack (at least for direct mentions).  Make sure that you have set up any personal reminders or alarms to be on time to your lab section and office hours.
2.  Prepare for each lab early.  Review the lab and any instructor's notes.  You will also need to prepare for homework ahead of office hours.
3.  Watch each other at work.  Drop in on more experienced CAs at the beginning of the semester and observe how they administer their classroom.
