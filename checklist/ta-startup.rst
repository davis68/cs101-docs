Checklist:  Startup for TAs
===========================

1.  Make sure that you receive notifications from the course Slack (at least for direct mentions).  Make sure that you have set up any personal reminders or alarms to be on time to your lab section and handy to monitor your Piazza hours and office hours.
2.  Prepare for each lab early.  Review the lab and any instructor's notes.  Make sure you have any necessary supplies on hand.  (Handouts will typically already be available, but you may need rewards.)  You will also need to prepare for homework ahead of office hours.
3.  Watch each other at work.  Drop in on more experienced TAs at the beginning of the semester and observe how they administer their classroom.
4.  Take some time to think about what teaching means to you.  Take a look at `Greg Wilson's Teaching Tech Together <http://teachtogether.tech/en/>`_ and `Qian and Lehman's Students’ Misconceptions and Other Difficulties in Introductory Programming <https://dl.acm.org/doi/10.1145/3077618>`.
