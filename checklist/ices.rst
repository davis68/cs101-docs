Checklist:  Course Feedback
===========================

There are two primary mechanisms for course feedback:  informal early feedback and ICES.

1.  Implement IEF as a flow partway through the semester.
2.  Request the following ICES questions, which give a better overview of large-course dynamics with lab sections than do the standard questions.

    1, 4, 29, 25, 47, 100, 112, 132, 143, 166, 184, 190, 241, 242, 248, 256, 291, 522, 524, 571, 576

3.  In addition, you should attempt to follow up with low-performing students halfway through the semester, using Doodle or meetwithme.com single sign-up times.
