Checklist:  i>clicker
=====================

To set up i>clicker on Linux:

1.  Update ``roster.txt`` (with the ``my.cs`` roster).
2.  Update ``RemoteID.csv`` (with Compass NetIDs).
3.  Back these up using Git.
