Checklist:  Course Shutdown
===========================

1.  Close the course website.
2.  Close Piazza.
3.  Compass sunsets naturally, but you can close it at ``Customization→Properties``.
4.  Deactivate unneeded accounts on Slack.
5.  Obtain any student examples for future demonstrations.

