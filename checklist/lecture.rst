Checklist:  Lectures
====================

Bring the following items to each lecture:

1.  i>clicker-compatible laptop
2.  Second microphone for ``translate.it`` accessibility

Prepare the following ahead of time:

1.  Slides
2.  Music selection (generally instrumental)
3.  IPython terminal
4.  ``translate.it`` URL
