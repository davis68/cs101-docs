Checklist:  Final Grading
=========================

1.  Pull all homework grades, quiz reflections, and bonus points together.
2.  Process exceptions.
3.  Post calculated results to Compass as numerical values and letter grades.
4.  Wait a day or two to resolve any final questions or issues.
5.  Post grades to campus.
6.  Handle James Scholar credit.

