Checklist:  Quiz
================

1.  Check flow dates or events in the flow file.
2.  Check the exam dates in RELATE.  (``Grading→Edit Exams``)
3.  Check the associated event dates in RELATE.  (``Staff→Admin Site→Events``)

Afterwards:

1.  Find the median and distribution.  Look for bimodality.
2.  Offer a reflection or curve activity if the median is lower than 75%.
3.  Examine the discrimination–difficulty plot to identify problematic and predictive questions.  Consider moving these.

