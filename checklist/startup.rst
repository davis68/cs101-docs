Checklist:  Course Startup
==========================

To set up an instance of CS 101:

1.  Create the following repos:
    1.  Labs
    2.  Lectures
    3.  Site
2.  Create a RELATE instance pointing at the site repo.
3.  Set up EWS for labs using ``nbgrader``.
4.  Update any short URLs, notably ``go.illinois.edu/cs101``.
5.  Request a Compass instance.  Include the lab sections separately rather than the lecture section.  You can carry over columns, but you can also set these up easily in Compass using the "Batch Create Column" feature.
6.  Create a Piazza instance and copy over the FAQs.
7.  Pull a copy of the ``my.cs`` roster on the first day.  Use it as a basis for comparison for late additions to the course.
8.  Print extra copies of lab handouts and leave them in the office hours notebook.
9.  Order any prizes such as badges.
10. Send invitations to the `course Slack instance <https://cs101-headquarters.slack.com/>`_ and meet with TAs.
11. Recruit CAs.

EWS Setup
---------

1.  Update ``/class/cs101/startup`` to the current semester.
2.  Set up ``/class/cs101/grading`` for pulling and deploying the labs.
3.  Install the most up-to-date version of Python along with the SciPy stack, Jupyter and friends, etc., in ``/class/cs101/software``.  Check perms, which should include ``o+r``.
4.  Start the ``schedule.sh`` script, which uses ``at`` to run itself on an hourly interval.

