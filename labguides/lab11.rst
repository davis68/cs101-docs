``lab11`` Instructor's Notes
============================

Worksheet
---------

-   `Handout <>`_ (requires Bitbucket access)

There is not a worksheet for students to turn in today.  There is a handout comparing Python and MATLAB coding which students should keep.

Jupyter
-------

This lab is team-based.

Students will complete code in MATLAB following instructions given in the notebook.  They should submit the notebook as this will include the modified files.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.


Grading
-------

Jupyter
   100%

   Basis:  Good-faith completion.  (We don't currently have an autograder framework set up for MATLAB, although I am interested in setting one up using either MATLAB or Octave.)
