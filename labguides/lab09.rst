``lab09`` Instructor's Notes
============================

Worksheet
---------

There is not a worksheet for students to turn in today.

Jupyter
-------

Students should complete this lab as a team.

This lab is a capstone on the earlier labs `language` and `encode`, and draws on both.  Encourage students to review those previous labs if they like.  By building a brute-force solver, students will see the power of contemporary desktop computers relative to much older techniques.  Students will also encounter basic concepts in cryptography and computer security.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.


Grading
-------

Jupyter
   100%

   Basis:  Autograder score.
