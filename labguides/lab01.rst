``lab01`` Instructor's Notes
============================

Worksheet
---------

-   `Worksheet <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab01/lab01.odt>`_ (requires Bitbucket access)

Students should complete the worksheet and the lab on their own this week.


Jupyter
-------

The worksheet contains a block which students should follow to set up and access Jupyter on their own machines.  If they do not follow this procedure properly, they won't be able to see the released lab or the *Assignments** tab.

Students will start to encounter new concepts in this lab which they have not seen in the lecture.  Generally, I introduce complex ideas in the lectures but leave some particular details (such as step size in string slicing) more to the labs.

Students need to ``Validate`` and ``Submit`` their notebook when they are done.  Some students may also need to complete ``lab00`` afterwards.


Review
------

Consider reviewing `lesson03` `py/loops`, in particular, `while` loops.


Grading
-------

Worksheet
   25%

   Basis:  Accuracy.

   1a.  ``str``

   1b.  ``float``

   1c.  ``float``

   1d.  ``float``

   1e.  ``str``

   1f.  ``complex``

   1g.  ``int``

   1h.  ``int``

   2a.  ``-a**b``

   2b.  ``a/b + a*b``

   2c.  ``1 + x/3 + (a+b)/x``

   2d.  ``int( 10**x / 5 )`` (also accept ``math.floor`` or clever use of ``//``)

   3a.  Valid.

   3b.  Valid.

   3c.  Invalid.  An operator is in the name.

   3d.  Invalid.  A space is in the name.

   3e.  Invalid.  An operator is in the name.

   3f.  Valid.

   3g.  Invalid.  A number begins the name.

Jupyter
   75%

   Basis:  Autograder score.
