``lab00`` Instructor's Notes
============================

Worksheet
---------

-   `Worksheet (version A) <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab00/lab00a.odt>`_ (requires Bitbucket access)
-   `Worksheet (version A solution) <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab00/lab00a-soln.odt>`_ (requires Bitbucket access)

-   `Worksheet (version B) <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab00/lab00b.odt>`_ (requires Bitbucket access)
-   `Worksheet (version B solution) <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab00/lab00b-soln.odt>`_ (requires Bitbucket access)

This lab is entirely conducted on paper and in person.  The learning objective is to invite students to develop computational thinking in contexts outside of programming, to better prepare them to compose code that achieves a particular goal.

Since many students may join the course after the first week of labs, please have them complete ``lab00`` during the subsequent week after ``lab01``, using a CA if necessary to coordinate activities.

The worksheet has four activities:

1.  A calculation which should be done on their own.  Collect answers and let them know the average, which in principle should be reasonably good.
2.  A drawing activity which they should do in pairs.  Each partner should be able to talk to the other, but should not be able to see their page.
3.  An activity done alone, then in pairs, then as a class.  There is a reward for the best guess here.
4.  Signing up for access to the course website.  Students may leave once they have done this, but they should turn in their sheets as a record of their attendance.

There are two different worksheets.  Please make sure that everyone in the section is using the same version of the worksheet.


Jupyter
-------

There is no online component of ``lab00``.


Grading
-------

Worksheet
   100%

   Basis:  Good-faith completion.
