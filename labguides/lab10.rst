``lab10`` Instructor's Notes
============================

Worksheet
---------

There is not a worksheet for students to turn in today.

Jupyter
-------

Students should complete this lab as a team.

This lab introduces image formatting as an array and some concepts involving censorship and image modification.  Understanding the pixelation algorithm can be challenging; you may want to illustrate it on the board.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.


Grading
-------

Jupyter
   100%

   Basis:  Autograder score.
