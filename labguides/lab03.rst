``lab03`` Instructor's Notes
============================

Worksheet
---------

-   `Worksheet <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab03/lab03.odt>`_ (requires Bitbucket access)

Students should complete the worksheet on their own this week.  Team discussion is permissible for the worksheet, however.


Jupyter
-------

Students should complete this lab as a team.

This lab is built around basic bioinformatics processing.  DNA and RNA transcription functions are built, and used in conjunction with a provided protein transcription function to process data.

Encourage students to read the lab carefully.  Many times students attempt to skip all text and just execute blocks of code, which creates confusion and misses the point of the lab exercise.  Students should have seen all concepts they need to employ in the lab already during the lecture, although the provided function `rna2amino` uses a `dict` which they have not yet encountered.

Each student needs to ``Validate`` and ``Submit`` his or her notebook when done.


Grading
-------

Worksheet
   25%

   Basis:  Accuracy.

   1.  ``open('skyscrapers.txt')`` or ``open('skyscrapers.txt','r')``

   2.  ``data_file.readlines()``

   3.  ``data_file.close()``

   4.  ``0`` or ``0.0``

   5.  ``line in data`` (or another index variable name)

   6.  ``values = line.split( ',' )``

   7.  ``height = float( values[ 0 ] )`` (all of these pieces are necessary)

   8.  ``sum += height`` or ``sum = sum + height``

Jupyter
   75%

   Basis:  Autograder score.
