``lab08`` Instructor's Notes
============================

Worksheet
---------

There is not a worksheet for students to turn in today.

Jupyter
-------

Students should complete this lab as a team.

This lab challenges students to solve various nuclear radioactive decay equations.  Some students become confused because different parts of the lab model different phenomena.  There is a mathematical appendix which is optional.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.


Grading
-------

Jupyter
   100%

   Basis:  Autograder score.
