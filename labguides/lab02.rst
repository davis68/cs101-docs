``lab02`` Instructor's Notes
============================

Worksheet
---------

-   `Worksheet <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab02/lab02.odt>`_ (requires Bitbucket access)

This worksheet is very involved and creative.  Expect a variety of answers as students think through how to solve and implement flowcharts in their code.

Students should complete the worksheet on their own this week.  Team discussion is permissible for the worksheet, however.


Jupyter
-------

This lab is built around coding special functions as Python functions.

Encourage students to read the lab carefully.  Many times students attempt to skip all text and just execute blocks of code, which creates confusion and misses the point of the lab exercise.  Students should have seen all concepts they need to employ in the lab already during the lecture.

Each student needs to ``Validate`` and ``Submit`` his or her notebook when done.


Grading
-------

Worksheet
   75%

   Basis:  Accuracy.

   1.  "The value before the function call is 5."

   2.  "x inside the function is 3."

   3.  "The value after the function call is 9."

   4.  Line 7

   5.  The variables names ``A``, ``B``, and ``C`` do not match their lower-case counterparts.

   6.  (Student action)

   7.  Program A

   8.  Program B

   9.  Program B

   10.  Clear variable names are preferred over terseness.  (Or something similar.)

   11.  Program D

   12.  Align variable names in similar patterns for ease of use.  (Or something similar.)
   
   Flowcharts component will have a new rubric.

Jupyter
   25%

   Basis:  Autograder score.
