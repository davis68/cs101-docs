``lab05`` Instructor's Notes
============================

Worksheet
---------

-   `Handout <>`_ (requires Bitbucket access)

There is not a worksheet for students to turn in today.  There is a handout on debugging strategies which students should keep.

Jupyter
-------

This lab contains a team portion and a solo portion.  You need to be prepared to time student work sessions.

Students are given code samples from a recent assignment and asked to debug, which in this case means diagnosing errors and correcting problems.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.


Grading
-------

Jupyter
   100%

   Basis:  Good-faith completion.  (We can't autograde arbitrary code.)
