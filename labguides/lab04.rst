``lab04`` Instructor's Notes
============================

Worksheet
---------

-   `Worksheet <https://bitbucket.org/davis68/cs101-labs/src/master/handouts/lab04/lab04.odt>`_ (requires Bitbucket access)

You will run the worksheet as an activity.  Prepare an encoded message using the program below and your section.  You may also introduce a message of your own device should you prefer.

``
messages = {
    'AYA': 'NEVER GONNA GIVE YOU UP',
    'AYB': 'NEVER GONNA LET YOU DOWN',
    'AYC': 'LIGHTS ALL ASKEW IN THE HEAVENS',
    'AYD': 'ALL THAT IS GOLD DOES NOT GLITTER',
    'AYE': 'REVEALED PREFERENCE',
    'AYF': 'NEVER GONNA RUN AROUND',
    'AYG': 'POETIC JUSTICE',
    'AYH': 'NEW SYSTEMS MEAN NEW PROBLEMS',
    'AYI': 'LEARN TO EMBRACE MISTAKES',
    'AYJ': 'NEVER GONNA GIVE YOU UP',
    'AYK': 'NEVER GONNA LET YOU DOWN',
    'AYL': 'LIGHTS ALL ASKEW IN THE HEAVENS',
    'AYM': 'ALL THAT IS GOLD DOES NOT GLITTER',
    'AYN': 'REVEALED PREFERENCE',
    'AYO': 'NEVER GONNA RUN AROUND',
    'AYP': 'POETIC JUSTICE',
    'AYQ': 'NEW SYSTEMS MEAN NEW PROBLEMS',
    'AYR': 'LEARN TO EMBRACE MISTAKES',
}

for key in messages:
    message = messages[ key ]
    print( f'{key}:  {message}' )
    for letter in message:
        print( str( bin( ord( letter ) ) )[ 2: ].zfill( 8 ),end=' ' )
    print()
``

Jupyter
-------

Students should complete this lab as a team.

This lab uses probabilistic associations of letter frequency to identify languages.

As usual, encourage students to read the lab carefully.  There is a block of examples at the bottom that isn't required but shows applications of the lab.  Students should complete it to get a feel for what they have accomplished in this lab.  Students should have seen all concepts they need to employ in the lab already during the lecture.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.

Students should complete this lab as a team.

This lab is the first part of a two-part series (the second being ``decode``, several weeks later).  In this lab, students will implement a World War Two-era cipher.  They are required to think about the mechanical system which implements the cipher.

As usual, encourage students to read the lab carefully.  You should be prepared to demonstrate how the cipher works on the board; use the graphics in "Rotor Cipher Machines" as a basis.  Students should have seen all concepts they need to employ in the lab already during the lecture.

One student in the team needs to ``Validate`` and ``Submit`` the team's notebook when they are done.  They should be sure that all NetIDs are correctly typed and included at the top of the notebook.


Grading
-------

Worksheet
   25%

   Basis:  Good-faith completion.

Jupyter
   75%

   Basis:  Autograder score.
