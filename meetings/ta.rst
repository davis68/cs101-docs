``ta0``
   TA Checklist
   ``lab00``
   Badges

``ta1``
   labs + jupyter
   ``lab00`` / ``lab01`` review
   ``lab02`` / ``lab03`` preview
   grading & compass [timeline,feedback,upload] (check submission sections)
   student policies
   course aides

``ta2``
   Qian on misconceptions
   ``lab03`` / ``lab04`` review
   ``lab05`` / ``lab06`` preview
   sending feedback
   remind students that they cannot be late, they lose points as well

``ta3``
   ``lab05`` / ``lab06``
   [feedback] never walk by a mistake/extreme ownership/mind your own business/handling cheating · ANSWER PIAZZA RAPIDLY MULTIPLE TIMES PER DAY
   the course is starting to bite now
   be punctual
   be proactive in office hours
   policies on piazza
   - use the queue

``ta4``
   _Teaching Tech Together_
   ``lab07`` / ``lab08``
   /how to distribute things back
   /fixing student notebook problems:  `[*]`, deleted cells, etc.
    - can we block modifications?  or border-highlight cells
   /grade check through 10/18
   /consistency on piazza---use codepost if they don't post actual code
   ×cheating

``ta5``  _Teaching Tech Together_
   pending topics
   → hidden cells in notebooks
   https://jupyter.org/jupyter-book/features/hiding.html
   → is prereq of calc 1 enforced?
   → `assert`s as fences in labs

``ta6``

``ta7``

``ta8``
   - ICES on Mon 12/09 09h30 ∵∴ Stephanie, Liqi, Priyank, Charlotte
   - What should we adjust in course operations?  Content?
      - student feedback:  lectures are too fast
      - active learning/peer instruction
      - rethink pair programming (peer assessment?  randomization?  everyone submits?  wrong netids are still a major problem!; uncommon pair labs, encourage programming in parallel together)
      - draconian on punctuality
      Bryan concerned about gender dynamics of groups
   - do labs together but write up separately?
   - submission of code notebook with quiz
   - tighter control of sharing screenshots via phones, etc.
   - announce cheating policy mid-semester as well
   - higher stakes on quizzes
   - better support for AYA
   - multi-lab project
   - questionnaire/subjective questions on physics etc.
   - quiz could allow explanation of what they are thinking
