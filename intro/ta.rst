.. highlight:: python

.. _latex:

Introduction for Teaching Assistants
====================================

.. module:: intro
   :synopsis: Introduction for Teaching Assistants

Course Administration
---------------------

-   `Course Website, go.illinois.edu/cs101 <https://go.illinois.edu/cs101>`_

-   `Piazza forum <https://piazza.com/illinois/spring2020/cs101/home>`_
.. TODO UPDATE

-   `Illinois Compass (gradebook) <https://compass2g.illinois.edu/>`_


Communications
--------------

-   `Slack channel cs101-headquarters <https://cs101-headquarters.slack.com>`_ is preferred for course-related discussions.  (Avoid using ``#general`` in favor of the appropriate channel for your comments.)

-   `cs101admin@cs.illinois.edu <mailto:cs101admin@cs.illinois.edu>`_ should be distributed to students and used as necessary for email-based communications.


Responsibilities
----------------

-   Instructor:

    -   lectures & lessons
    -   lab development
    -   homework
    -   quizzes
    -   courseware and infrastructure

-   Course administrator:

    -   operations and scheduling
    -   student exceptions (absence, DRES, etc.)

-   Teaching assistants:

    -   holding office hours
    -   hosting and grading labs
    -   answering questions on Piazza
    -   composing new course materials (mainly exercises)
    -   reviewing course materials ahead of time
    -   *being prepared to answer student questions* (including MATLAB)

-   Course aides:

    -   supporting TAs in lab responsibilities and office hours
    -   holding office hours
    -   *being prepared to answer student questions* (including MATLAB)


Facilities
----------

-   **Foellinger Auditorium**:  lectures 12h00–12h50 Monday and Wednesday

-   **L416 DCL**:  student labs

-   **EWS labs in 0220 Siebel**:  office hours starting week 2 of class

-   **2229 Siebel**:  instructor's office

-   **Suite 2100 NCSA**:  instructor's office hours


Staff Meeting
-------------

TAs, the instructor, and the course administrator meet together biweekly on Tuesday at 5:00 p.m.  These meetings are required and everyone is expected to be punctual.


Lab Sections
------------

Students should complete their labs in their own section.  **Exceptions should be rare and must be approved by the course administrator.**  The deadline for submission is the end of class.  After ``lab01``, this is a hard deadline.

Labs will typically consist of three sections:

1.  (30 min.)  A question-and-answer/review session to make sure students understand difficult material.  (I suggest preparing for this ahead of time using the instructor's notes and your sense of what your students struggle the most with.)

2.  (~20 min.)  A printed worksheet

3.  (~60 min.)  An online component, either a RELATE flow or a Jupyter notebook

Once we start using Jupyter notebooks in ``lab01``, students will need to run the following command:

``/class/cs101/startup``

This script will set up each student's account so that they are able to access the correct repository for fetching and submitting assignments.  (This step is also necessary for guest account users.)  Once they have run the script, they should close and reopen the command line window.  A student only needs to run this script once.

Students then access notebooks via the command line by typing `jupyter notebook`.  A browser will open with a notebook session, including options to fetch and submit assignments.  Submitted assignments will be automatically retrieved at the hour ending a lab session, so students should submit promptly.

Labs will be made available to TAs beforehand (see below for details).  You should review and understand the labs, as well as report any difficulties or errors as soon as possible for correction prior to release to students.  Hard copies of the worksheets will be left in the room by the instructor or the `AYA` TA.

For the first few weeks of the semester, we encourage TAs to visit each other's lab sections to see best practices, provide feedback, and cross-pollinate good ideas for lab management.

-   A queue management tool for lab questions is available online at `go.illinois.edu/cs101-labq <https://go.illinois.edu/cs101-labq>`_.  You will need to open the queue when you begin your lab (and close it when done).



Grading
-------

Jupyter notebooks are autograded using `nbgrader` <https://nbgrader.readthedocs.io/en/stable/>`_, although worksheets need to be graded by hand.  TAs should take the worksheets with them at the end of their section for grading and return them to students at the beginning of the next lab session.

A service running on EWS autogrades lab notebooks every couple of hours.  Each TA is responsible for extracting and posting grades for his or her section.  The gradebook is a SQLite file located on EWS at ``/class/cs101/grading/gradebook.db``.  There is a script for extracting grades available with the `CS 101 course tools <https://github.com/davis68/CS101Scripts>`_.

1.  Clone the course tools repo to your own EWS home directory.
2.  Provide a roster from Compass.  Modify the ``config.yml`` file to accommodate this.
3.  You can extract grades for your section using:

    ``python3 ultimateGradeCalculator.py #``

    where `#` is the lab number.  The `-c [Y,N]` flag lets you extract collaborators for partnered labs; it is on by default.
4.  You should provide an output CSV, also from Compass.  This can be the same file as the roster, and Compass lets you only upload one column at a time even if several are available in the gradebook file.
5.  Students may have added themselves to the wrong section.  Correct this as soon as possible.  Furthermore, you may change the scaling factor in `config.yml` to `1.5` for weeks when there is a handout for credit as well.

**Grading should be completed and posted online by the subsequent Friday (one week).**  Update the appropriate Slack channel that your grading is complete; the course administrator will then make grades visible.  TAs should return notebook feedback to students on the subsequent Friday as well.

Emergency procedures if labs are unavailable
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

-   Scenario #1:  Labs are locked.  Call Engineering IT at 217-333-1313 or ``ews@illinois.edu``.

-   Scenario #2:  Computer infrastructure is down (home directories unavailable, etc.).  Call EngrIT and notify Neal.  Do not dismiss students for at least 15 minutes.

    Do not unplug or power off a machine.  Power it on and tell EWS as much information as possible about what's wrong by submitting a help ticket.

-   Scenario #3:  Labs are unavailable in Jupyter.  DM the instructor immediately on Slack.  Do not dismiss students for at least 15 minutes.

How to set up a per-section gradebook view in Compass
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. In Compass, open the Full Grade Center.
2. Click ``Manage→Smart Views``.
3. Click the ``Create Smart View`` button.
4. Set up your smart view and press ``Submit``.


Timeline
--------

Labs are available on the Linux EWS filesystem at ``/class/cs101/grading``.  You should take the opportunity to review the lab and handout ahead of time.  The labs will be released to students on Monday, and will generally be collaborative (from ``lab02`` onwards).  I will autograde the online portion, but you are required to grade any handouts (these are generally for completeness but comments to students are welcome).  You are responsible for processing and inputting all grades by the Friday *after* the lab completes, to allow time for students with exceptions to do the lab.  After grades are released on that Friday, you should also return feedback to students; there is an HTML file made by the autograder as well as the handouts you grade.

To send feedback:
1.  Generate HTML feedback reports by running `nbgrader feedback lab01` in your `AYA` directory.
1.  In one terminal, run `python -m smtpd -n -c DebuggingServer localhost:1025`
2.  In another terminal, run `python send_feedback.py lab01` (inside of your `AYA` etc. directory).


Resources
---------

TAs have the following resources available to them:

-   Guest accounts ``cs101-01`` through ``cs101-05``.  These will be cleared regularly, so encourage students to save work they wish to keep to their own device or upload the work to Box.  Students will additionally have to run ``/class/cs101/startup`` for the current section.  The password is ``darkseed76`` and should *not* be distributed to students (type it in for them when logging in).

-   Computers may be logged out by pressing ``Ctrl``+``Alt``+``Backspace`` if students leave without logging off.

-   The EWS help line is 217-333-1313 (3-1313 on campus lines).  The email address for the help desk is `ews@illinois.edu <mailto:ews@illinois.edu>`_.


Office Hours
------------

Office hours are focused on helping students think critically through problems—homework, approved labs, and general questions.  As homework assignments are due on Wednesday, office hours will be held Monday through Wednesday.  (CAs may run office hours without a TA present if necessary.)

-   A queue management tool for office hours is available online at `go.illinois.edu/cs101-ohq <https://go.illinois.edu/cs101-ohq>`_.  You will need to open the queue when you begin office hours (and close it when done).


Piazza Forum
------------

TAs will be assigned hours of responsibility for responding to student questions on Piazza.  We have experimented with different policies on Piazza (to encourage student answers, etc.).  Our policy is to **respond to questions quickly**, so please stay on top of questions asked during your assigned hours.

The course has a *two-hour policy*, which means that if a student has worked on a problem for two hours without making progress, they can just post their code privately and ask for help.  Realistically, if they haven't solved it themselves in two hours, they aren't going to solve it in ten.  So to save on frustration, at that point they can just post their code privately to Piazza, invoke the two-hour rule, and a TA will look at it and figure out what's wrong (or at least, the major thing wrong at that point).  It doesn't mean that you solve all of their problems, but that you help them get past that point.


Final Reminders
---------------

Think of the course as a machine which we are building to a design specification.  This machine consists of people and processes.  We could automate almost every part of this class, but beyond a certain point that won't make any sense.  Our target outcome is students who can perform to a certain level and have a good time doing so.  We need TAs to patiently work with students, and that is what I hope you will spend most of your time doing—not grading or dealing with logistics.  (Writing and reviewing course materials is fine, though.)

You are expected to attend every weekly staff meeting and carry out your other responsibilities.  Failure to do so can lead to revocation of your TA contract (and thus your tuition waiver).  You are additionally expected to model the utmost academic integrity for your students.

You are expected to be here through the end of the semester, including grading the final exam.  (However, much of grading the final can be carried out online.)  If you need time off (*e.g.*, for an interview), you are still responsible for your section, and will have to coordinate another TA trading responsibility yourself (but report the switch to the course administrator).


Next Steps
----------

-   Enroll in all course-related services (RELATE, Piazza, Slack).

-   Check ``lab00`` and related materials.  Office hours will start the second week of class.

-   Make sure that the course administrator has your schedule so we can assign lab, office hour, and Piazza responsibilities.
