.. highlight:: python

.. _latex:

Introduction for Course Aides
=============================

.. module:: intro
   :synopsis: Introduction for Course Aides

Course Administration
---------------------

-   `Course Website, go.illinois.edu/cs101 <https://go.illinois.edu/cs101>`_

-   `Piazza forum <https://piazza.com/illinois/fall2019/cs101/home>`_
.. TODO UPDATE
-   `Illinois Compass (gradebook) <https://compass2g.illinois.edu/>`_


Communications
--------------

-   `Slack channel cs101-headquarters <https://cs101-headquarters.slack.com>`_ is preferred for course-related discussions.  (Avoid using ``#general`` in favor of the appropriate channel for your comments.)

-   `cs101admin@cs.illinois.edu <mailto:cs101admin@cs.illinois.edu>`_ should be distributed to students and used as necessary for email-based communications.


Responsibilities
----------------

-   Instructor:

    -   lectures & lessons
    -   lab development
    -   homework
    -   quizzes
    -   courseware and infrastructure

-   Course administrator:

    -   operations and scheduling
    -   student exceptions (absence, DRES, etc.)

-   Teaching assistants:

    -   holding office hours
    -   hosting and grading labs
    -   answering questions on Piazza
    -   composing new course materials (mainly exercises)
    -   reviewing course materials ahead of time
    -   *being prepared to answer student questions* (including MATLAB)

-   Course aides:

    -   supporting TAs in lab responsibilities and office hours
    -   holding office hours


Facilities
----------

-   **Foellinger Auditorium**:  lectures 09h00–09h50 Monday and Wednesday

-   **2229 Siebel**:  instructor's office

-   **L416 DCL**:  student labs

-   **EWS labs in 0220 Siebel**:  office hours starting week 2 of class


Lab Sections
------------

Students should complete their labs in their own section.  **Exceptions should be rare and must be approved by the course administrator.**  The deadline for submission is the end of class.  After ``lab01``, this is a hard deadline.

Labs will typically consist of three sections:

1.  (30 min.)  A question-and-answer/review session to make sure students understand difficult material (I suggest preparing for this ahead of time.)

2.  (~20 min.)  A printed worksheet

3.  (~60 min.)  An online component, either a RELATE flow or a Jupyter notebook

Once we start using Jupyter notebooks in ``lab01``, students will need to run the following command:

``/class/cs101/startup``

This interactive script will ask for the student's section so that they are able to access the correct repository for fetching and submitting assignments.  (This step is also necessary for guest account users.)

Students then access notebooks via the command line by typing `jupyter notebook`.  A browser will open with a notebook session, including options to fetch and submit assignments.  Submitted assignments will be automatically retrieved at the hour ending a lab session, so students should submit promptly.

Labs will be made available to TAs one week beforehand (see below for details).  You should review and understand the labs, as well as report any difficulties or errors as soon as possible for correction prior to release to students.  Hard copies of the worksheets will be left in the room by the instructor or the `AYA` TA.

For the first few weeks of the semester, we encourage TAs to visit each other's lab sections to see best practices, provide feedback, and cross-pollinate good ideas for lab management.

-   A lab queueing tool is available online at `go.illinois.edu/cs101-labq <https://go.illinois.edu/cs101-labq>`_.


**Emergency procedures if labs are unavailable**:

-   Scenario #1:  Labs are locked.  Call EngrIT at 217-333-1313.

-   Scenario #2:  Computer infrastructure is down (home directories unavailable, etc.).  Call EngrIT and notify Neal.  Do not dismiss students for at least 15 minutes.

    Do not unplug or power off a machine.  Power it on and tell EWS as much information as possible about what's wrong by submitting a help ticket.

-   Scenario #3:  Labs are unavailable in Jupyter.  Slack or text Neal immediately at 217-666-1040.  Do not dismiss students for at least 15 minutes.


Timeline
--------

Most labs are written already, so I will push them regularly to EWS in ``/class/cs101/grading``.  You should take the opportunity to review the lab and handout ahead of time.  The labs will be released on Monday, and will generally be collaborative for students (``lab02`` and later).


Resources
---------

CAs have the following resources available to them:

-   Guest accounts ``cs101-01`` through ``cs101-05``.  These will be cleared regularly, so encourage students to save work they wish to keep to their own device or upload the work to Box.  Students will additionally have to run ``/class/cs101/startup`` for the current section.  The password is ``darkseed76`` and should *not* be distributed to students (type it in for them when logging in).

-   Computers may be logged out by pressing ``Ctrl``+``Alt``+``Backspace`` if students leave without logging off.

-   The EWS help line is 217-333-1313 (3-1313 on campus lines).  The email address for the help desk is `ews@illinois.edu <mailto:ews@illinois.edu>`_.


Office Hours
------------

Office hours are focused on helping students think critically through problems—homework, approved labs, and general questions.  As homework assignments are due on Wednesday, office hours will be held Monday through Wednesday.  (CAs may run office hours without a TA present if necessary.)

-   An office hours queueing tool is available online at `go.illinois.edu/cs101-ohq <https://go.illinois.edu/cs101-ohq>`_.


Final Reminders
---------------

Think of the course as a machine which we are building to a design specification.  This machine consists of people and processes.  We could automate almost every part of this class, but beyond a certain point that won't make any sense.  Our target outcome is students who can perform to a certain level and have a good time doing so.  We need CAs to patiently work with students, and that is what I hope you will spend most of your time doing—not grading or dealing with logistics.  (Writing and reviewing course materials is fine, though.)

You are expected to be here through the last week of courses.  If you need time off (*e.g.*, for an interview), you are still responsible for your section, and will have to coordinate another CA trading responsibility yourself (but report the switch to the course administrator).


Next Steps
----------

-   Enroll in all course-related services (RELATE, Piazza, Slack).

-   Check the next lab and any related materials.  Office hours will start the second week of class.

-   Make sure that the course administrator has your schedule so we can assign lab and office hour responsibilities.

