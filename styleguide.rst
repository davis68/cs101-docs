.. raw:: html
  <style>
  .red {color:#E40066;};
  .yellow {color:#EAC435;};
  .green {color:#03CEA4;};
  .blue {color:#345995;};
  .grey {color:#5D5D5D;};
  </style>

.. role:: red
.. role:: yellow
.. role:: green
.. role:: blue
.. role:: grey


Style Guide for Contributions
=============================

Colors
------

We use a few visually distinct colors to designate values in this course.  These are different enough in tone that color-blind users should generally be able to distinguish them, but be careful with green and red in particular.

Typefaces
---------

- Ubuntu for headings, slide text, and labels.
- Ubuntu Light for emphasis.
- Ubuntu Mono for code and buttons.
- Linux Libertine for running text.

"CS101", for instance, is often stylized using bold-face Ubuntu and Ubuntu Light.

Colors
~~~~~~

:red:`#E40066	228    0  102 ██████████`
:yellow:`#EAC435	234  196   53 ██████████`
:green:`#03CEA4	  3  206  164 ██████████`
:blue:`#345995	 52   89  149 ██████████`
:grey:`#5D5D5D	 93   93   93 ██████████`


Diagrams & Flowcharts
---------------------

Drawings should have line weights comparable to the thickness of the Ubuntu Mono labels.  Labels should be typeset using Ubuntu Mono, bold, all caps, separated by 1 pt expansion.

Graphics should be generated using the LaTeX modules TikZ and PGFPlots.

- `flowchart example <>`_
- `drawing example <>`_


RELATE Flows and Pages
----------------------

RELATE uses Bootstrap for buttons and features.  There is a CSS file available at the current semester’s course website at ``files/modules/style.html``.  Buttons may be defined using the classes ``btn``, ``btn-sm``, ``btn-condensed``, and the particular color styles ``py`` ``exrc`` ``ml`` with default ``btn-primary``.

The CSS file ``style.html`` file which is included in normal flow files and pages using

::
  {% from "yaml-macros.jinja" import indented_include %}
  {{ indented_include("modules/style.html", 8) }}

Inside of CBTF, the CDN we use to apply the correct fonts is blocked.  Instead, we refer to ``style-cbtf.html`` which otherwise formats the page correctly but doesn't load the webfonts for Ubuntu etc.

