Root Cause Analysis, 2019-04-23
===============================

Initial Observations
--------------------

1.  I was informed by a student that entering ``nan`` on a ``TextQuestion`` on RELATE would always yield 100%.
2.  I was further informed that students or former students were telling people to just use this information to game the quiz.

Actions Taken
-------------

1.  I corresponded immediately with the main developer of RELATE, Andreas Kloeckner, and he patched the bug.
2.  I had a TA review submissions looking for those who answered ``nan``.  (At this point, I was unaware that one could obtain ``nan`` as a legitimate mistake.)  These students were submitted to FAIR.
3.  I realized from student feedback that there was also a contingent of students who were using tools like ``scipy.optimize.minimize`` incorrectly and obtaining `nan` from their failure.  (Primarily this seems to have occurred from those use values less than zero with a logarithm, which of course is undefined.)  These students were cleared of any suspicion, but did not receive points for the actually-incorrect ``nan``.
4.  There was a group of students who was involved in cheating—that is, actively telling others about the bug and actively using it.  These students received a zero for ``quiz4`` and are not eligible for reflection makeup points on it.

Recommendations
---------------

1.  Clarify the use of ``minimize`` and similar tools in cases of failure.
2.  Fuzz the grading system more thoroughly (that is, try values which should cause errors).
3.  Consider reminding students about the restricted domain of certain functions like ``log``.
